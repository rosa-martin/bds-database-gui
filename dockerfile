FROM maven:3.8.3-jdk-11-slim AS build
COPY pom.xml app/pom.xml
COPY src /app/src
WORKDIR /app
RUN ls
RUN mvn clean install -DskipTests && \
cp target/bds-database-gui-*.jar /app/bds-database.jar
ENTRYPOINT ["java", "-Dprism.order=sw", "-Djdk.gtk.version=2", "-jar", "/app/bds-database.jar"]


