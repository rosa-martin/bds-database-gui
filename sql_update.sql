-- this document cotains commands to update database so that it can work correctly with the GUI application
-- commands are to be parsed into a querry

--adding username and password to employee table

alter table lib_db.employee
add column username varchar(45);
alter table lib_db.employee
add column password varchar(100);
insert into lib_db.employee (password)
values ('$2a$12$68ch9bro0IDNdegKaztWqeDlIO4zxT9u4fIDQp6cQck0KvZQregmm');
insert into lib_db.employee (username)
values('martin55')
where firstname='Martin';

--you can also fill in other employees with other usernames but they are not necesarry

alter table lib_db.book_has_author
add constraint fk_delete_in_book_has_author
foreign key (book_idbook)
references lib_db.book(idbook)
on delete cascade;

alter table lib_db.genre_has_book
add constraint fk_delete_in_book_has_author
foreign key (book_idbook)
references lib_db.book(idbook)
on delete cascade;


-- the dummy table for SQL injection simulation

create schema if not exists sql_injection_test;

create table if not exists sql_injection_test.person1(
	idperson 	serial primary key not null,
	first_name	varchar(45) not null,
	surname		varchar(45) not null,
	username	varchar(25) not null,
	password	varchar(100) not null
);

insert into sql_injection_test.person1
values (1 ,'Johanes', 'Guttenberg', 'JGut', '123456');
insert into sql_injection_test.person1
values (2, 'Martin', 'Luther', 'MLnotK', '123456');
insert into sql_injection_test.person1
values (3, 'Albert', 'Einstein', 'xXx_PhysicsB0i_xXx', '123456');
insert into sql_injection_test.person1
values (4, 'Ted', 'Kaczynski', 'unabomber_official', '123456');
insert into sql_injection_test.person1
values (5, 'Daniel', 'Craig', 'zeroZeroSeven', '123456');
insert into sql_injection_test.person1
values (6, 'Ernest', 'Khalimov', 'gigaChad', '123456');
insert into sql_injection_test.person1
values (7, 'Sheev', 'Palpatine', 'TheSenate123', '123456');
insert into sql_injection_test.person1
values (8, 'Arthur', 'Morgan', 'morgan-arthur', '123456');
insert into sql_injection_test.person1
values (9, 'Carl', 'Sagan', 'livesOnSmallDot', '123456');
insert into sql_injection_test.person1
values (10, 'David', 'Davidson', 'sonOfDavid', '123456');
insert into sql_injection_test.person1
values (11, 'Eduard', 'Bernstein', 'BEduard', '123456');
insert into sql_injection_test.person1
values (12, 'Geralt', 'Rivia', 'Witcher', '123456');

select * from sql_injection_test.person1;

