package org.but.feec.database;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.but.feec.database.exceptions.ExceptionHandler;

import java.net.URL;

/**
 * @author Martin Rosa
 */
public class App extends Application {

    private FXMLLoader loader;
    private VBox mainStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            loader = new FXMLLoader(getClass().getResource("Login.fxml"));
            System.out.println(loader.getLocation());
            mainStage = loader.load();

            primaryStage.setTitle("Log in");
            Scene scene = new Scene(mainStage);
            setUserAgentStylesheet(STYLESHEET_MODENA);
//            String myStyle = getClass().getResource("css/myStyle.css").toExternalForm();
//            scene.getStylesheets().add(myStyle);
//
//            primaryStage.getIcons().add(new Image(App.class.getResourceAsStream("logos/vut.jpg")));
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception ex) {

           ExceptionHandler.handleException(ex);
        }
    }

}