package org.but.feec.database.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.database.api.PersonView;
import org.but.feec.database.data.PersonRepository;
import org.but.feec.database.api.PersonAuthView;
import org.but.feec.database.exceptions.ExceptionHandler;

import java.util.List;

public class AuthService {
    private PersonRepository personRepository;

    public AuthService(PersonRepository personRepository) {
        this.personRepository = personRepository;

    }

    private PersonAuthView findPersonByUname(String uname) {
        return personRepository.findPersonByUsername(uname);
    }

    public boolean authenticate(String username, String password) throws Exception {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        PersonAuthView pav = findPersonByUname(username);
        if (pav == null) {
            ExceptionHandler.handleException(new Exception("Provided username is not found."));
        }

        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), pav.getPassword());
        return result.verified;
        /*if(pav.getPassword().equals(password)){     //not actuall develomplent, just toying around
            return true;
        } else{
            System.out.println(pav.getPassword());
            System.out.println(password);
            return false;
        }*/
    }

    public boolean authenticate2(String username, String password) throws Exception {
        if(username == null || username.isEmpty()){
            return false;
        }

        List<PersonView> pav = personRepository.findByUsername2(username);
        if(pav == null){
            ExceptionHandler.handleException(new Exception("Provided username is not found."));
        }

        try{
            if(pav.get(0).getPassword().equals(password)){
                return true;
            } else {
                return false;
            }
        } catch(NullPointerException e){
            e.printStackTrace();
            return false;
        }

    }

    //sql inejction

    public List<PersonView> personView(String uname){
        return personRepository.findByUsername2(uname);
    }
}
