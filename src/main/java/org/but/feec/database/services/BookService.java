package org.but.feec.database.services;

import org.but.feec.database.api.BookAddView;
import org.but.feec.database.api.BookDetailedView;
import org.but.feec.database.api.BookView;
import org.but.feec.database.data.BookRepository;
import java.awt.print.Book;
import java.util.List;

public class BookService {

    private BookRepository br;

    public BookService(BookRepository br){
        this.br = br;
    }

    public List<BookView> getBookSimpleView(){
        return br.getBooks();
    }

    public List<BookDetailedView> getBookDetailedView(String name) {
        return br.getBooksDetailed(name);
    }

    public void editBook(BookDetailedView bookDetailedView){
        br.updateBook(bookDetailedView);
    }

    public void addBook(BookAddView bookAddView){
        br.addBook(bookAddView);
    }

    public void deleteBook(int id){
        br.deleteBook(id);
    }

}
