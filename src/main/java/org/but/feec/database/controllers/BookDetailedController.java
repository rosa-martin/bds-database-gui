package org.but.feec.database.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.database.App;
import org.but.feec.database.api.BookDetailedView;
import org.but.feec.database.api.BookView;
import org.but.feec.database.data.BookRepository;
import org.but.feec.database.exceptions.ExceptionHandler;
import org.but.feec.database.services.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.control.*;

import java.sql.SQLException;
import java.util.List;

public class BookDetailedController {

    private static final Logger logger = LoggerFactory.getLogger(BookDetailedController.class);

    @FXML
    private TableColumn<BookDetailedView, Integer> id;
    @FXML
    private TableColumn<BookDetailedView, String> book_name;
    @FXML
    private TableColumn<BookDetailedView, Integer> n_o_pages;
    @FXML
    private TableColumn<BookDetailedView, String> state;
    @FXML
    private TableColumn<BookDetailedView, Integer> edition;
    @FXML
    private TableColumn<BookDetailedView, String> publisher_name;
    @FXML
    private TableColumn<BookDetailedView, String> published;
    @FXML
    private TableColumn<BookDetailedView, String> isbn;
    @FXML
    private TableView<BookDetailedView> detailedBooks;


    private BookRepository br;
    private BookService bs;
    private String name;
    private Stage stage;


    public BookDetailedController(String name) {
        this.name = name;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() throws SQLException{
        br = new BookRepository();
        bs = new BookService(br);

        id.setCellValueFactory(new PropertyValueFactory<BookDetailedView, Integer>("id"));
        book_name.setCellValueFactory(new PropertyValueFactory<BookDetailedView, String >("name"));
        n_o_pages.setCellValueFactory(new PropertyValueFactory<BookDetailedView, Integer>("n_o_pages"));
        state.setCellValueFactory(new PropertyValueFactory<BookDetailedView, String>("state"));
        edition.setCellValueFactory(new PropertyValueFactory<BookDetailedView, Integer>("edition"));
        publisher_name.setCellValueFactory(new PropertyValueFactory<BookDetailedView, String>("publisher"));
        published.setCellValueFactory(new PropertyValueFactory<BookDetailedView, String>("published"));
        isbn.setCellValueFactory(new PropertyValueFactory<BookDetailedView, String>("isbn"));

        ObservableList<BookDetailedView> observableBookList = initializeBookData(name);
        detailedBooks.setItems(observableBookList);
        detailedBooks.getSortOrder().add(id);
        detailedBooks.sort();

        initializeTableViewSection();

        logger.info("Detailed view initialized");

    }


    public void initializeTableViewSection(){
        MenuItem edit = new MenuItem("Edit");
        MenuItem delete = new MenuItem("Delete");
        edit.setOnAction((ActionEvent event) -> {
            BookDetailedView bookDetailedView = detailedBooks.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(App.class.getResource("bookEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(bookDetailedView);
                stage.setTitle("Edit");

                BookEditController bookEditController = new BookEditController();
                bookEditController.setStage(stage);
                loader.setController(bookEditController);

                Scene scene = new Scene(loader.load(), 700, 500);
                stage.setScene(scene);

                stage.show();

            } catch(Exception e){
                ExceptionHandler.handleException(e);
            }
        });

        delete.setOnAction((ActionEvent event) -> {
            BookDetailedView bookDetailedView = detailedBooks.getSelectionModel().getSelectedItem();
            try{
                bs.deleteBook(bookDetailedView.getId());
                handleRefreshButton();
            } catch (Exception e){
                ExceptionHandler.handleException(e);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().add(delete);
        detailedBooks.setContextMenu(menu);
    }




    public ObservableList<BookDetailedView> initializeBookData(String name) {
        List<BookDetailedView> bookDetailedViewList = bs.getBookDetailedView(name);
        return FXCollections.observableArrayList(bookDetailedViewList);
    }


    public void handleRefreshButton(){
        ObservableList<BookDetailedView> observableBookList = initializeBookData(name);
        detailedBooks.setItems(observableBookList);
        detailedBooks.refresh();
        detailedBooks.getSortOrder().add(id);
        detailedBooks.sort();
    }




}
