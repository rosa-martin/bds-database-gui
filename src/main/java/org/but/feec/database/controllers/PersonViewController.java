package org.but.feec.database.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.database.api.BookView;
import org.but.feec.database.api.PersonView;
import org.but.feec.database.data.PersonRepository;
import org.but.feec.database.services.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class PersonViewController {
    private static final Logger logger = LoggerFactory.getLogger(BookControler.class);

    @FXML
    private TableColumn<PersonView, String> username;
    @FXML
    private TableColumn<PersonView, String> firstName;
    @FXML
    private TableColumn<PersonView, String> surname;
    @FXML
    private TableColumn<PersonView, String> password;
    @FXML
    private TableView<PersonView> persons;

    private PersonRepository pr;
    private AuthService as;
    private Stage stage;
    private String uname;

    public PersonViewController(String uname){
        this.uname = uname;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize(){
        pr = new PersonRepository();
        as = new AuthService(pr);

        username.setCellValueFactory(new PropertyValueFactory<PersonView, String>("username"));
        firstName.setCellValueFactory(new PropertyValueFactory<PersonView, String>("first_name"));
        surname.setCellValueFactory(new PropertyValueFactory<PersonView, String>("surname"));
        password.setCellValueFactory(new PropertyValueFactory<PersonView, String>("password"));

        ObservableList<PersonView> personViews = initializePersonData();
        persons.setItems(personViews);
        persons.sort();

    }

    public ObservableList<PersonView> initializePersonData(){
        List<PersonView> personView = as.personView(uname);
        return FXCollections.observableArrayList(personView);
    }

}
