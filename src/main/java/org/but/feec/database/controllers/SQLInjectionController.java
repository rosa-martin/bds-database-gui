package org.but.feec.database.controllers;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.database.App;
import org.but.feec.database.data.PersonRepository;
import org.but.feec.database.services.AuthService;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Optional;

public class SQLInjectionController {
    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Button submit;

    private ValidationSupport validation;
    private PersonRepository pr;
    private AuthService as;
    private Stage stage;

    public SQLInjectionController() {

    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        pr = new PersonRepository();
        as = new AuthService(pr);
        validation = new ValidationSupport();

        validation.registerValidator(usernameField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(passwordField, Validator.createEmptyValidator("Field must not be empty"));


    }

    public void handleAuthentization() throws Exception {
        String username = usernameField.getText();
        String password = passwordField.getText();

        boolean isAuthenticated = as.authenticate2(username, password);
        if(isAuthenticated){
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(App.class.getResource("personView.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Profile");

            PersonViewController controller = new PersonViewController(username);
            controller.setStage(stage);
            loader.setController(controller);

            Scene scene = new Scene(loader.load(), 600, 400);
            stage.setScene(scene);

            stage.show();

            Stage oldStage = (Stage) submit.getScene().getWindow();
            oldStage.close();


        }
    }

}
