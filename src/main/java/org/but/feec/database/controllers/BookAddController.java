package org.but.feec.database.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.database.api.BookAddView;
import org.but.feec.database.data.BookRepository;
import org.but.feec.database.services.BookService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Optional;

public class BookAddController {

    private static final Logger logger = LoggerFactory.getLogger(BookControler.class);

    @FXML
    private TextField nameTextField;
    @FXML
    private TextField n_o_pagesTextField;
    @FXML
    private TextField stateTextField;
    @FXML
    private TextField editionTextField;
    @FXML
    private TextField publisherTextField;
    @FXML
    private TextField publishedTextField;
    @FXML
    private TextField isbnTextField;
    @FXML
    private Button addBook;
    @FXML
    private GridPane bookData;

    private BookRepository br;
    private BookService bs;
    private ValidationSupport validation;
    public Stage stage;

    public BookAddController() {

    }

    public void addStage(Stage stage){
        this.stage = stage;
    }

    @FXML
    public void initialize() throws SQLException {
        br = new BookRepository();
        bs = new BookService(br);
        validation = new ValidationSupport();

        validation.registerValidator(nameTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(n_o_pagesTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(stateTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(editionTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(publisherTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(publishedTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(isbnTextField, Validator.createEmptyValidator("Field must not be empty"));


        logger.info("BookEditController initialized");


    }

    @FXML
    public void handleAddBookButton() {
        BookAddView bookAddView = new BookAddView();
        bookAddView.setName(nameTextField.getText());
        bookAddView.setN_o_pages(Integer.valueOf(n_o_pagesTextField.getText()));
        bookAddView.setState(stateTextField.getText());
        bookAddView.setEdition(Integer.valueOf(editionTextField.getText()));
        bookAddView.setPublisher(publisherTextField.getText());
        bookAddView.setPublished(publishedTextField.getText());
        bookAddView.setIsbn(isbnTextField.getText());

        bs.addBook(bookAddView);

        bookAddedSuccessfullyMessage();

    }

    public void bookAddedSuccessfullyMessage(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Book Added Confirmation");
        alert.setHeaderText("Book was successfully added.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
