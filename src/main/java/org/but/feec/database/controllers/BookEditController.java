package org.but.feec.database.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.database.api.BookDetailedView;
import org.but.feec.database.api.BookView;
import org.but.feec.database.data.BookRepository;
import org.but.feec.database.exceptions.ExceptionHandler;
import org.but.feec.database.services.BookService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.scene.control.TextField;

import java.sql.SQLException;
import java.util.Optional;

public class BookEditController {

    private static final Logger logger = LoggerFactory.getLogger(BookDetailedController.class);

    @FXML
    private TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField n_o_pagesTextField;
    @FXML
    private TextField stateTextField;
    @FXML
    private TextField editionTextField;
    @FXML
    private TextField publisherTextField;
    @FXML
    private TextField publishedTextField;
    @FXML
    private TextField isbnTextField;
    @FXML
    private Button saveChanges;
    @FXML
    private GridPane bookData;

    private BookRepository br;
    private BookService bs;
    private ValidationSupport validation;
    public Stage stage;

    public void setStage(Stage stage){
        this.stage = stage;
    }

    public BookEditController() {

    }

    @FXML
    public void initialize() throws SQLException {
        br = new BookRepository();
        bs = new BookService(br);
        validation = new ValidationSupport();

        validation.registerValidator(idTextField, Validator.createEmptyValidator("Field must not be empty"));
        idTextField.setEditable(false);
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(n_o_pagesTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(stateTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(editionTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(publisherTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(publishedTextField, Validator.createEmptyValidator("Field must not be empty"));
        validation.registerValidator(isbnTextField, Validator.createEmptyValidator("Field must not be empty"));
        isbnTextField.setEditable(false);

        loadBookData();

        logger.info("BookEditController initialized");
    }

    private void loadBookData(){
        Stage stage = this.stage;
        if(stage.getUserData() instanceof BookDetailedView){
            BookDetailedView bookDetailedView = (BookDetailedView) stage.getUserData();
            idTextField.setText(String.valueOf(bookDetailedView.getId()));
            nameTextField.setText(bookDetailedView.getName());
            n_o_pagesTextField.setText(String.valueOf(bookDetailedView.getN_o_pages()));
            stateTextField.setText(bookDetailedView.getState());
            editionTextField.setText(String.valueOf(bookDetailedView.getEdition()));
            publisherTextField.setText(bookDetailedView.getPublisher());
            publishedTextField.setText(bookDetailedView.getPublished());
            isbnTextField.setText(bookDetailedView.getIsbn());
        }
    }

    @FXML
    private void handleSaveChanges(){
        BookDetailedView bookDetailedView = new BookDetailedView();
        bookDetailedView.setId(Integer.valueOf(idTextField.getText()));
        bookDetailedView.setName(nameTextField.getText());
        bookDetailedView.setN_o_pages(Integer.valueOf(n_o_pagesTextField.getText()));
        bookDetailedView.setState(stateTextField.getText());
        bookDetailedView.setEdition(Integer.valueOf(editionTextField.getText()));
        bookDetailedView.setPublisher(publisherTextField.getText());
        bookDetailedView.setPublished(publishedTextField.getText());
        bookDetailedView.setIsbn(isbnTextField.getText());
        bs.editBook(bookDetailedView);

        bookEditConfirmation();

    }

    private void bookEditConfirmation(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Book Edited Confirmation");
        alert.setHeaderText("Your book was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}
