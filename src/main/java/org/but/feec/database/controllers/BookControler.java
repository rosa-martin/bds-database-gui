package org.but.feec.database.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.database.App;
import org.but.feec.database.api.BookDetailedView;
import org.but.feec.database.api.BookView;
import org.but.feec.database.data.BookRepository;
import org.but.feec.database.exceptions.ExceptionHandler;
import org.but.feec.database.services.BookService;
import org.controlsfx.control.PropertySheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.print.Book;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class BookControler {

    private static final Logger logger = LoggerFactory.getLogger(BookControler.class);

    @FXML
    private TableColumn<BookView, Integer> count;
    @FXML
    private TableColumn<BookView, String> name;
    @FXML
    private TableColumn<BookView, Integer> n_o_pages;
    @FXML
    private TableColumn<BookView, String> isbn;
    @FXML
    private TableView<BookView> books;

    private BookService bs;
    private BookRepository br;

    public BookControler() {

    }

    @FXML
    public void initialize() throws SQLException {
        br = new BookRepository();
        bs = new BookService(br);

        count.setCellValueFactory(new PropertyValueFactory<BookView, Integer>("count"));
        name.setCellValueFactory(new PropertyValueFactory<BookView, String>("name"));
        n_o_pages.setCellValueFactory(new PropertyValueFactory<BookView, Integer>("n_o_pages"));
        isbn.setCellValueFactory(new PropertyValueFactory<BookView, String>("isbn"));

        ObservableList<BookView> observableBookList = initializeBookData();
        books.setItems(observableBookList);
        books.getSortOrder().add(name);
        books.sort();

        initializeTableViewSelection();

        logger.info("BookControler intialized");
    }

    public ObservableList<BookView> initializeBookData(){
        List<BookView> books = bs.getBookSimpleView();
        return FXCollections.observableArrayList(books);
    }

    @FXML
    public void handleAddBookButton(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(App.class.getResource("bookAdd.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Add Book");

            BookAddController controller = new BookAddController();
            controller.addStage(stage);
            loader.setController(controller);

            Scene scene = new Scene(loader.load(), 1000, 800);
            stage.setScene(scene);

            stage.show();

        } catch (IOException e){
            ExceptionHandler.handleException(e);
        }
    }

    public void initializeTableViewSelection(){
        MenuItem detailedView = new MenuItem("Detailed book view");
        detailedView.setOnAction((ActionEvent event) -> {
            BookView bookView = books.getSelectionModel().getSelectedItem();
            try {
                String name = bookView.getName();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("booksDetailed.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Detailed view");

                BookDetailedController controller = new BookDetailedController(name);
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 1000, 800);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });



        ContextMenu menu = new ContextMenu();
        menu.getItems().add(detailedView);
        books.setContextMenu(menu);
    }

    @FXML
    public void handleRefreshButton(){
        ObservableList<BookView> observableBookList = initializeBookData();
        books.setItems(observableBookList);
        books.refresh();
        books.getSortOrder().add(name);
        books.sort();
    }



}
