package org.but.feec.database.controllers;


import javafx.scene.Scene;
import javafx.stage.Stage;
import org.but.feec.database.App;
import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import org.but.feec.database.api.BookView;
import org.but.feec.database.data.PersonRepository;
import org.but.feec.database.exceptions.ExceptionHandler;
import org.but.feec.database.services.AuthService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @FXML
    public Label usernameLabel;
    @FXML
    public Label passwordLabel;
    @FXML
    public Label vutLogo;
    @FXML
    private Button signInButton;
    @FXML
    private Button sqlInjectionButton;
    @FXML
    private TextField usernameTextfield;
    @FXML
    private PasswordField passwordTextField;

    private PersonRepository personRepository;
    private AuthService authService;

    private ValidationSupport validation;

    public LoginController() {
    }

    @FXML
    private void initialize() throws SQLException {
        GlyphsDude.setIcon(signInButton, FontAwesomeIcon.SIGN_IN, "1em");
        GlyphsDude.setIcon(usernameLabel, FontAwesomeIcon.USER, "2em");
        GlyphsDude.setIcon(passwordLabel, FontAwesomeIcon.USER_SECRET, "2em");
        usernameTextfield.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });
        passwordTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });

        sqlInjectionButton.setOnKeyPressed(event -> {
            try {
                initializeSQLInjection();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        initializeServices();
        initializeValidations();

        logger.info("LoginController initialized");
    }

    private void initializeValidations() {
        validation = new ValidationSupport();
        validation.registerValidator(usernameTextfield, Validator.createEmptyValidator("The username must not be empty."));
        validation.registerValidator(passwordTextField, Validator.createEmptyValidator("The password must not be empty."));
        signInButton.disableProperty().bind(validation.invalidProperty());
    }

    private void initializeServices() throws SQLException {
        personRepository = new PersonRepository();
        authService = new AuthService(personRepository);
    }


    public void signInActionHandler(ActionEvent event) {
        handleSignIn();
    }

    private void handleSignIn() {
        String username = usernameTextfield.getText();
        String password = passwordTextField.getText();

        try {
            boolean isAuthenticated = authService.authenticate(username, password);

            if (isAuthenticated){
                showBooks();
            } else {
               authErrorDialog();
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public void initializeSQLInjection() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(App.class.getResource("sql-injection.fxml"));
        Stage stage = new Stage();
        stage.setTitle("SQL Injection Test");

        SQLInjectionController sqlInjectionController = new SQLInjectionController();
        sqlInjectionController.setStage(stage);
        loader.setController(sqlInjectionController);

        Scene scene = new Scene(loader.load(), 650, 350);
        stage.setScene(scene);

        stage.show();




    }



    private void showBooks() throws IOException {
        FXMLLoader fxml = new FXMLLoader();
        fxml.setLocation(App.class.getResource("books.fxml"));
        Scene scene = new Scene(fxml.load(), 1050, 600);
        Stage stage = new Stage();
        stage.setTitle("Books");
        stage.setScene(scene);

        Stage oldStage = (Stage) signInButton.getScene().getWindow();
        oldStage.close();

        stage.show();

    }

    private void showInvalidPaswordDialog() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Unauthenticated");
        alert.setHeaderText("The user is not authenticated");
        alert.setContentText("Please provide a valid username and password");//ww  w . j  a  va2s  .  co  m

        alert.showAndWait();
    }


    private void authConfirmDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Logging confirmation");
        alert.setHeaderText("You were successfully logged in.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            System.out.println("ok clicked");
        } else if (result.get() == ButtonType.CANCEL) {
            System.out.println("cancel clicked");
        }
    }

    public void handleOnEnterActionPassword(ActionEvent dragEvent) {
        handleSignIn();
    }

    private void authErrorDialog(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setWidth(700);
        alert.setHeight(700);
        alert.setTitle("Authentication failed");
        TextArea ta = new TextArea("Authentication failed!");
        ta.setWrapText(true);
        ta.setEditable(false);
        alert.getDialogPane().setContent(ta);
        alert.setResizable(true);
        alert.showAndWait();
    }
}
