package org.but.feec.database.api;

public class BookAddView {
    private String name;
    private int n_o_pages;
    private String state;
    private String isbn;
    private int edition;
    private String published;
    private String publisher;

    public BookAddView() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getN_o_pages() {
        return n_o_pages;
    }

    public void setN_o_pages(int n_o_pages) {
        this.n_o_pages = n_o_pages;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getEdition() {
        return edition;
    }

    public void setEdition(int edition) {
        this.edition = edition;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
