package org.but.feec.database.api;

public class BookView {
    private int count;
    private String name;
    private int n_o_pages;
    private String isbn;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getN_o_pages() {
        return n_o_pages;
    }

    public void setN_o_pages(int n_o_pages) {
        this.n_o_pages = n_o_pages;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
