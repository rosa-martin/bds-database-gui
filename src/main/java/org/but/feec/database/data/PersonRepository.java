package org.but.feec.database.data;

import org.but.feec.database.api.PersonAuthView;
import org.but.feec.database.api.PersonView;
import org.but.feec.database.config.DataSourceConfig;
import org.but.feec.database.exceptions.ExceptionHandler;
import org.postgresql.jdbc.PgStatement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {


    public PersonAuthView findPersonByUsername(String uname) {
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("SELECT username, password " +
                "FROM lib_db.employee e " +
                "WHERE e.username = ?;")){
            pst.setString(1, uname);
            try(ResultSet rs = pst.executeQuery()){
                if (rs.next()){
                    return convertToPersonAuth(rs);
                }
            }
        } catch (SQLException e){
            ExceptionHandler.handleException(e);
        }

        return null;
    }

    private PersonAuthView convertToPersonAuth(ResultSet rs) throws SQLException{
        PersonAuthView person = new PersonAuthView();
        person.setUname(rs.getString("username"));
        person.setPassword(rs.getString("password"));
        return person;
    }

    //SQL injection test
    public List<PersonView> findByUsername2(String uname){

        try(Connection conn = DataSourceConfig.getConnection(); Statement stmt = conn.createStatement();){
            String statement = "SELECT first_name, username, surname, password FROM sql_injection_test.person1 WHERE username='" + uname + "';";
            try(ResultSet rs = stmt.executeQuery(statement);){
                List<PersonView> personViewList = new ArrayList<PersonView>();
                while(rs.next()){
                    personViewList.add(toPersonView(rs));
                }
                System.out.println(personViewList);
                return personViewList;
            }


        }catch (SQLException e){
            ExceptionHandler.handleException(e);
            return null;
        }
    }


    public PersonView toPersonView(ResultSet rs) throws SQLException{
        PersonView personView = new PersonView();
        personView.setUsername(rs.getString("username"));
        personView.setFirst_name(rs.getString("first_name"));
        personView.setSurname(rs.getString("surname"));
        personView.setPassword(rs.getString("password"));
        return personView;
    }


}
