package org.but.feec.database.data;

import org.but.feec.database.api.BookAddView;
import org.but.feec.database.api.BookDetailedView;
import org.but.feec.database.api.BookView;
import org.but.feec.database.config.DataSourceConfig;
import org.but.feec.database.exceptions.DataAccessException;
import org.but.feec.database.exceptions.ExceptionHandler;

import javax.xml.crypto.Data;
import java.awt.print.Book;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BookRepository {


    public List<BookView> getBooks() {

        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("SELECT count(name) count, name, n_o_pages, isbn " +
                "FROM lib_db.book b join lib_db.isbn i ON b.isbn_idisbn = i.idisbn " +
                "GROUP BY name, n_o_pages, isbn;")) {
            try(ResultSet rs = pst.executeQuery()){
                List<BookView> bookViewList = new ArrayList<BookView>();
                while (rs.next()){
                    //System.out.println("Entry");
                    bookViewList.add(toBookView(rs));
                }
                return bookViewList;
            }

        } catch(Exception e){
            ExceptionHandler.handleException(e);
            return null;
        }
    }

    public List<BookDetailedView> getBooksDetailed(String name) {
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("SELECT b.idbook id, b.name book_name, b.n_o_pages, b.state, b.which_edition edition, p.name publisher_name, b.published, i.isbn " +
                "FROM lib_db.book b JOIN lib_db.isbn i ON b.isbn_idisbn=i.idisbn " +
                "JOIN lib_db.publisher p ON b.publisher_idpublisher=p.idpublisher " +
                "WHERE b.name=?;")){
            pst.setString(1, name);
            try(ResultSet rs = pst.executeQuery()){
                List<BookDetailedView> bookViewList = new ArrayList<BookDetailedView>();
                while (rs.next()){
                    bookViewList.add(toBookDetailedView(rs));
                }
                return bookViewList;
            }

        } catch(Exception e){
            ExceptionHandler.handleException(e);
            return null;
        }
    }

    public void updateBook(BookDetailedView bookDetailedView){
            String update = "UPDATE lib_db.book " +
                "SET name=?, n_o_pages=?, state=?, which_edition=?, published=? " +
                "WHERE idbook=?";
            String existenceCheck = "SELECT name FROM lib_db.book WHERE idbook=?";
            String findPublisherId = "SELECT idpublisher FROM lib_db.publisher WHERE name=?;";
            String updatePublisher_idPublisher = "UPDATE lib_db.book " +
                    "SET publisher_idpublisher=? " +
                    "WHERE idbook=?;";

            int idPublisher = 0;

            try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement(update, Statement.RETURN_GENERATED_KEYS)){
                pst.setString(1, bookDetailedView.getName());
                pst.setInt(2, bookDetailedView.getN_o_pages());
                pst.setString(3, bookDetailedView.getState());
                pst.setInt(4, bookDetailedView.getEdition());
                pst.setDate(5, Date.valueOf(bookDetailedView.getPublished()));
                pst.setInt(6, bookDetailedView.getId());

                try{
                    conn.setAutoCommit(false);
                    try(PreparedStatement ps = conn.prepareStatement(existenceCheck, Statement.RETURN_GENERATED_KEYS)){
                        ps.setInt(1, bookDetailedView.getId());
                        ps.execute();
                    } catch (SQLException e){
                        ExceptionHandler.handleException(e);
                    }

                    try(PreparedStatement ps = conn.prepareStatement(findPublisherId, Statement.RETURN_GENERATED_KEYS)){
                        ps.setString(1, bookDetailedView.getPublisher());
                        ResultSet rs = ps.executeQuery();
                        if(rs.next()){
                            idPublisher = rs.getInt("idpublisher");
                        } else {
                            idPublisher = addPublisher(bookDetailedView.getPublisher());
                        }


                    } catch (SQLException e){
                        ExceptionHandler.handleException(e);
                    }

                    try(PreparedStatement publisherStatement = conn.prepareStatement(updatePublisher_idPublisher)){
                        publisherStatement.setInt(1, idPublisher);
                        publisherStatement.setInt(2, bookDetailedView.getId());
                        publisherStatement.executeUpdate();
                    } catch (SQLException e){
                        ExceptionHandler.handleException(e);
                    }

                    int amountOfAffectedRows = pst.executeUpdate();
                    if(amountOfAffectedRows == 0){
                        throw new DataAccessException("This book for edit does not exist");
                    }

                    conn.commit();
                } catch(SQLException e){
                    conn.rollback();
                    ExceptionHandler.handleException(e);
                } finally {
                    conn.setAutoCommit(true);
                }

            } catch(SQLException e){
                ExceptionHandler.handleException(e);
            }

    }

    public void addBook(BookAddView bookAddView){
        String insert = "INSERT INTO lib_db.book (state, n_o_pages, isbn_idisbn, which_edition, published, name, publisher_idpublisher) " +
                "VALUES (?,?,?,?,?,?,?)";
        String lookForPublisher = "SELECT idpublisher " +
                "FROM lib_db.publisher " +
                "WHERE name=?";
        String lookForIsbn = "SELECT idisbn " +
                "FROM lib_db.isbn " +
                "WHERE isbn=?";
        int isbn_idisbn = 0, publisher_idpublisher = 0;
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement isbnStatement = conn.prepareStatement(lookForIsbn)){
            isbnStatement.setString(1, bookAddView.getIsbn());
            ResultSet rs = isbnStatement.executeQuery();
            if(rs.next()){
                isbn_idisbn = rs.getInt("idisbn");
            } else {
                isbn_idisbn = addIsbn(bookAddView.getIsbn());
            }

        } catch(SQLException e){
            ExceptionHandler.handleException(e);
        }

        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement publisherStatement = conn.prepareStatement(lookForPublisher)){
            publisherStatement.setString(1, bookAddView.getPublisher());
            ResultSet rs = publisherStatement.executeQuery();
            if(rs.next()){
                publisher_idpublisher = rs.getInt("idpublisher");
            } else {
                publisher_idpublisher = addPublisher(bookAddView.getPublisher());
            }

        } catch(SQLException e){
            ExceptionHandler.handleException(e);
        }

        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS)){
            pst.setString(1, bookAddView.getState());
            pst.setInt(2, bookAddView.getN_o_pages());
            pst.setInt(3, isbn_idisbn);
            pst.setInt(4, bookAddView.getEdition());
            pst.setDate(5, Date.valueOf(bookAddView.getPublished()));
            pst.setString(6, bookAddView.getName());
            pst.setInt(7, publisher_idpublisher);
            int alteredStuff = pst.executeUpdate();
            if(alteredStuff == 0){
                throw new DataAccessException("Adding a book has failed");
            }
        } catch (SQLException e){
            ExceptionHandler.handleException(e);
        }


    }

    public void deleteBook(int id){
        String delete = "DELETE FROM lib_db.book " +
                "WHERE idbook=?;";
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement(delete, Statement.RETURN_GENERATED_KEYS)){
            pst.setInt(1, id);
            int amountOfRowsChanged = pst.executeUpdate();
            if(amountOfRowsChanged == 0){
                throw new DataAccessException("Deleting was unsuccessful");
            }
        } catch(SQLException e){
            e.printStackTrace();
        }
    }

    public BookView toBookView(ResultSet rs) throws SQLException{
        BookView book = new BookView();
        book.setCount(rs.getInt("count"));
        book.setName(rs.getString("name"));
        book.setN_o_pages(rs.getInt("n_o_pages"));
        book.setIsbn(rs.getString("isbn"));

        return book;
    }

    public BookDetailedView toBookDetailedView(ResultSet rs) throws SQLException {
        BookDetailedView book = new BookDetailedView();
        book.setId(rs.getInt("id"));
        book.setName(rs.getString("book_name"));
        book.setN_o_pages(rs.getInt("n_o_pages"));
        book.setState(rs.getString("state"));
        book.setEdition(rs.getInt("edition"));
        book.setPublisher(rs.getString("publisher_name"));
        book.setPublished(rs.getString("published"));
        book.setIsbn(rs.getString("isbn"));

        return book;

    }

    public int addIsbn(String isbn) {
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("INSERT INTO lib_db.isbn (isbn) " +
                "VALUES (?);")){
            pst.setString(1, isbn);
            pst.executeUpdate();

        } catch(SQLException e){
            e.printStackTrace();
            return 0;
        }

        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("SELECT idisbn " +
                "FROM lib_db.isbn " +
                "WHERE isbn=?;")){
            pst.setString(1, isbn);
            ResultSet rs = pst.executeQuery();
            rs.next();
            return rs.getInt("idisbn");
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

    public int addPublisher(String publisher){
        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("INSERT INTO lib_db.publisher (name)" +
                "VALUES (?)")){
            pst.setString(1, publisher);
            pst.executeUpdate();
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }

        try(Connection conn = DataSourceConfig.getConnection(); PreparedStatement pst = conn.prepareStatement("SELECT idpublisher " +
                "FROM lib_db.publisher " +
                "WHERE name=?")){
            pst.setString(1, publisher);
            ResultSet rs = pst.executeQuery();
            rs.next();
            return rs.getInt("idpublisher");
        } catch (SQLException e){
            e.printStackTrace();
            return 0;
        }
    }

}
